<?php

	session_start();
	
	if (!isset($_SESSION['loggedIn'])) {
		header('Location: index.php');
		exit();
	}
	
	require_once "connect.php";

	$connection = pg_connect("$host $port $dbname $credentials");

	$team = $_POST['team'];

	$sql = sprintf("INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Priority, Assigned_To, Issue) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s');",
			pg_escape_string($connection, $_SESSION['first_name']),
			pg_escape_string($connection, $_SESSION['last_name']),
			pg_escape_string($connection, $_SESSION['sex']),
			pg_escape_string($connection, $_SESSION['age']),
			pg_escape_string($connection, $_SESSION['priority']),
			pg_escape_string($connection, $team),
			pg_escape_string($connection, $_SESSION['medical_issue']));
	$result = pg_query($connection, $sql);
	
	if(!$result){
		$_SESSION['err'] = 'NOT OK';
		$_SESSION['assignment'] = '<span style="color:red">'.pg_last_error($connection).'</span>';
		header('Location: assign.php');
	} else {
		$_SESSION['assignment'] = '<span style="color:green">Record created successfully!</span>';
		header('Location: assign.php');
	}
	pg_close($connection);

?>