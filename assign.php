<?php

	session_start();
	
	if (!isset($_SESSION['loggedIn'])) {
		header('Location: index.php');
		exit();
	}

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>The emergency room at Cuckoo's Nest Hospital</title>
	<meta name="description" content="bla bla bla" />
	<meta name="keywords" content="la la la" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body>

	<form id="assign_form" action="assign2.php" method="post" >
		<div id="assign">
			<?php
			
				function checkError($result, $connection) {
					if(!$result){
						$_SESSION['assignment'] = '<span style="color:red">'.pg_last_error($connection).'</span>';
						echo $_SESSION['assignment'];
						unset($_SESSION['assignment']);
						exit();
					}
				}
			
				require_once "connect.php";

				$connection = pg_connect("$host $port $dbname $credentials");
				
				if (!$connection) {
					$_SESSION['assignment'] = '<span style="color:red">Error : Unable to open database.</span>';
					echo $_SESSION['assignment'];
					unset($_SESSION['assignment']);
					exit();
				} else {
				
					$sql = sprintf("SELECT Condition FROM Priority WHERE ID = '%d'",
						pg_escape_string($connection, $_SESSION['priority']));
					$result = pg_query($connection, $sql);
					checkError($result, $connection);
					
					$row = pg_fetch_row($result);
					$p = $row[0];
					
					$sql = sprintf("SELECT Name FROM MedicalIssue WHERE ID = '%d'",
						pg_escape_string($connection, $_SESSION['medical_issue']));
					$result = pg_query($connection, $sql);
					checkError($result, $connection);
					
					$row = pg_fetch_row($result);
					$mi = $row[0];
					
					$separator = str_repeat('&nbsp;', 5);
				
					if (isset($_SESSION['first_name'])) {
						$text = $separator.$_SESSION['first_name'].$separator.$_SESSION['last_name'].$separator.$_SESSION['sex'].$separator.$_SESSION['age'].$separator.$p.$separator.$mi;
					} else {
						$text = $separator.'NONE';
						$competent = array_fill(0, 5, 'disabled');
						$_SESSION['competent'] = $competent;
						$disable_button = 'disabled';
					}
					
					$d1 = $_SESSION['competent'][0];
					$d2 = $_SESSION['competent'][1];
					$d3 = $_SESSION['competent'][2];
					$d4 = $_SESSION['competent'][3];
					$d5 = $_SESSION['competent'][4];
				
					echo<<<END
					<div id="patient_info">
						Assign the patient:<span style="color: darkred">$text</span>
					</div>
					<div id="team_choice">
						<input id="ALFA" type="radio" name="team" value="1" required $d1 />
						<label for="ALFA">ALFA</label>
						<input id="BETA" type="radio" name="team" value="2" $d2 />
						<label for="BETA">BETA</label>
						<input id="GAMMA" type="radio" name="team" value="3" $d3 />
						<label for="GAMMA">GAMMA</label>
						<input id="DELTA" type="radio" name="team" value="4" $d4 />
						<label for="DELTA">DELTA</label>
						<input id="EPSILON" type="radio" name="team" value="5" $d5 />
						<label for="EPSILON">EPSILON</label>
						<input id="button" type="submit" value="ASSIGN" $disable_button/>
						
					</div>
END;

				}

				if (isset($_SESSION['assignment'])) {
					echo $_SESSION['assignment'];
					unset($_SESSION['assignment']);
					if (isset($_SESSION['err'])) {
						unset($_SESSION['err']);
						exit();
					}
					//$med = $_SESSION['medical_issue'];	
					echo "<script type='text/javascript'>window.parent.location.href='tnur.php?m_issue={$_SESSION['medical_issue']}'</script>";
				}
				
			?>
			
		</div>
	</form>
	
</body>

</html>