<?php

	session_start();
	
	if (!isset($_SESSION['loggedIn'])) {
		header('Location: index.php');
		exit();
	}
	
	$_SESSION['procedures'] = $_POST['procedures'];
	$_SESSION['drugs'] = $_POST['drugs'];
	$_SESSION['outcome'] = $_POST['outcome'];
	
	function checkError($result, $connection) {
		if(!$result){
			$_SESSION['submit'] = '<span style="color:red">'.pg_last_error($connection).'</span>';
			header('Location: phy.php');
			exit();
		}
	}
	
	require_once "connect.php";

	$connection = pg_connect("$host $port $dbname $credentials");

	if (!$connection) {
		$_SESSION['submit'] = '<span style="color:red">Error : Unable to open database.</span>';
		header('Location: phy.php');
		exit();
	} else {
		
		$result = pg_query($connection, "BEGIN;");
		checkError($result, $connection);
		
		$result = pg_query($connection, "SET TRANSACTION ISOLATION LEVEL Serializable;");
		checkError($result, $connection);
		
		$sql = sprintf("UPDATE Treatment SET Outcome = '%s' WHERE Patient_ID = '%s';",
			pg_escape_string($connection, $_SESSION['outcome']),
			pg_escape_string($connection, $_SESSION['patient_id']));
		$result = pg_query($connection, $sql);
		checkError($result, $connection);
		
		foreach($_SESSION['procedures'] as $procedure) {
			$sql = sprintf("INSERT INTO UtilizedProcedure(Patient_ID, Procedure_ID) VALUES('%s', '%s');",
				pg_escape_string($connection, $_SESSION['patient_id']), 
				pg_escape_string($connection, $procedure));
			$result = pg_query($connection, $sql);
			checkError($result, $connection);
		}
		
		foreach($_SESSION['drugs'] as $drug) {
			$sql = sprintf("INSERT INTO ProvidedDrug(Patient_ID, Drug_ID) VALUES('%s', '%s');",
				pg_escape_string($connection, $_SESSION['patient_id']),
				pg_escape_string($connection, $drug));
			$result = pg_query($connection, $sql);
			checkError($result, $connection);
		}
		
		// VitalStatistics    
		// 1 BOTH SOME PROCEDURE(s) AND DRUG(s) PROVIDED    
		// 2 NO DRUGS PROVIDED (ONLY PROCEDURE(s))
		// 3 NO PROCEDURES UTILIZED (ONLY DRUG(s))
		// 4 NEITHER PROCEDURE(s) NOR DRUG(s) = MALINGER!!! 
		if (!(empty($_SESSION['procedures'])) AND !(empty($_SESSION['drugs']))) {
			$v_stat = 'VitalStatistics1';
			
			$sql = sprintf("SELECT DISTINCT Procedure FROM $v_stat WHERE ID = '%s'",
				pg_escape_string($connection, $_SESSION['patient_id']));
			$result = pg_query($connection, $sql);
			checkError($result, $connection);
			
			$procedures = array(); 
			while($row = pg_fetch_row($result)) {
				array_push($procedures, $row[0]);
			}
			
			$sql = sprintf("SELECT DISTINCT Drug FROM $v_stat WHERE ID = '%s'",
				pg_escape_string($connection, $_SESSION['patient_id']));
			$result = pg_query($connection, $sql);
			checkError($result, $connection);
			
			$drugs = array(); 
			while($row = pg_fetch_row($result)) {
				array_push($drugs, $row[0]);
			}
			
		} else if (!(empty($_SESSION['procedures'])) AND empty($_SESSION['drugs'])) {
			$v_stat = 'VitalStatistics2';
			
			$sql = sprintf("SELECT DISTINCT Procedure FROM $v_stat WHERE ID = '%s'",
				pg_escape_string($connection, $_SESSION['patient_id']));
			$result = pg_query($connection, $sql);
			checkError($result, $connection);
			
			$procedures = array(); 
			while($row = pg_fetch_row($result)) {
				array_push($procedures, $row[0]);
			}
			
			$drugs = array();
			
		} else if (empty($_SESSION['procedures']) AND !(empty($_SESSION['drugs']))) {
			$v_stat = 'VitalStatistics3';
			
			$procedures = array(); 
			
			$sql = sprintf("SELECT DISTINCT Drug FROM $v_stat WHERE ID = '%s'",
				pg_escape_string($connection, $_SESSION['patient_id']));
			$result = pg_query($connection, $sql);
			checkError($result, $connection);
			
			$drugs = array(); 
			while($row = pg_fetch_row($result)) {
				array_push($drugs, $row[0]);
			}
			
		} else {
			$v_stat = 'VitalStatistics4';
			
			$procedures = array(); 
	
			$drugs = array(); 
			
		}
		$sql = sprintf("SELECT DISTINCT First_Name, Last_Name, Sex, Age, Issue, Had_To_Wait, Cost FROM $v_stat WHERE ID = '%s'",
			pg_escape_string($connection, $_SESSION['patient_id']));
		$result = pg_query($connection, $sql);
		checkError($result, $connection);
		if (pg_num_rows($result) > 0) {
			$row = pg_fetch_row($result);
			$_SESSION['vital_statistics']['first_name'] = $row[0];
			$_SESSION['vital_statistics']['last_name'] = $row[1];
			$_SESSION['vital_statistics']['sex'] = $row[2];
			$_SESSION['vital_statistics']['age'] = $row[3];
			$_SESSION['vital_statistics']['medical_issue'] = $row[4];
			$_SESSION['vital_statistics']['had_to_wait'] = $row[5];
			$_SESSION['vital_statistics']['cost'] = $row[6];
		}
		
		$_SESSION['vital_statistics']['outcome'] = $_SESSION['outcome'];
		
		$vital_statistics = '';
		foreach($_SESSION['vital_statistics'] as $k => $value) {
			$vital_statistics .= $value.' | ';
		}
		if (empty($procedures)) {
			$vital_statistics .= 'NONE ';
		} else {
			foreach($procedures as $k => $value) {
				$vital_statistics .= $value.' ';
			}
		}
		$vital_statistics .= '| ';
		if (empty($procedures)) {
			$vital_statistics .= 'NONE ';
		} else {
			foreach($drugs as $k => $value) {
				$vital_statistics .= $value.' ';
			}
		}
		$vital_statistics .= "\n";
		
		
		$file = 'patients.txt';
		if (filesize($file) == 0) {
			$header = "FirstName | LastName | Sex | Age | MedicalIssue | Awaited (min.) | TotalCost (USD) | Outcome | Procedure(s) | Drug(s) \n";
			file_put_contents($file, $header, FILE_APPEND | LOCK_EX);
		}
		file_put_contents($file, $vital_statistics, FILE_APPEND | LOCK_EX);
	
		$sql = sprintf("DELETE FROM Patient WHERE ID = '%s';",
			pg_escape_string($connection, $_SESSION['patient_id']));
		$result = pg_query($connection, $sql);
		checkError($result, $connection);
		
		$result = pg_query($connection, "COMMIT;");
		checkError($result, $connection);
		
		pg_close($connection);
		
		$_SESSION['submit'] = '<span style="color:green">Report created successfully!</span>';
		header('Location: phy.php');
	}
	
?>