<?php

	session_start();
	
	if (!isset($_SESSION['loggedIn'])) {
		header('Location: index.php');
		exit();
	}

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>The emergency room at Cuckoo's Nest Hospital</title>
	<meta name="description" content="bla bla bla" />
	<meta name="keywords" content="la la la" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<script type="text/javascript" src="jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="script.js"></script>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body style="background-image: url('img/rockywall.png')">

<?php

	require_once "connect.php";

	$connection = pg_connect("$host $port $dbname $credentials");
	
	$team = $_GET['team'];
	$medical_issue = $_SESSION['medical_issue'];
	
	$sql = sprintf("SELECT * FROM CompetentToDealWith WHERE Team_ID = '%s' AND Issue_ID = '%s'",
			pg_escape_string($connection, $team),
			pg_escape_string($connection, $medical_issue));
	$result = pg_query($connection, $sql);
	
	if (pg_num_rows($result) > 0) { /*TEAM IS COMPETENT*/
	
		switch ($team) {
			case 1:
				$_SESSION['competent'][0] = '';
				break;
			case 2:
				$_SESSION['competent'][1] = '';
				break;
			case 3:
				$_SESSION['competent'][2] = '';
				break;
			case 4:
				$_SESSION['competent'][3] = '';
				break;
			case 5:
				$_SESSION['competent'][4] = '';
				break;
			default:
				echo 'No such team!';
		}
		
		$sql = sprintf("SELECT first_name, last_name, priority, waiting_time FROM Priorityqueue WHERE emergency_team_id = '%s'", 
				pg_escape_string($connection, $team));
		$result = pg_query($connection, $sql);
		if (pg_num_rows($result) > 0) {
			$i = 1;
			while($row = pg_fetch_row($result)){
				switch ($row[2]) {
					case 1:
						$p = 'SATISFACTORY';
						break;
					case 2:
						$p = 'GUARDED';
						break;
					case 3:
						$p = 'SERIOUS';
						break;
					case 4:
						$p = 'CRITICAL';
						break;
					case 5:
						$p = 'GRAVE';
						break;
					default:
						echo 'No such priority!';
				}
				if ($i == 1) echo "<div id='blinker'>$i $row[0] $row[1] $p</div>";
				else echo "$i $row[0] $row[1] $p $row[3] min. <br />";
				$i++;
			}
		} else {
			echo 'EMPTY QUEUE';
		}
		
	} else {
		echo 'NOT COMPETENT TO DEAL WITH';
	}
	pg_close($connection);
	
	/*echo '<pre>';
	var_dump($_SESSION);
	echo '</pre>';*/
	
?>

</body>

</html>