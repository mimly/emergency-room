<?php

	session_start();
	
	if ((isset($_SESSION['loggedIn'])) and ($_SESSION['loggedIn'] == true)) {
		if ($_SESSION['user'] == 'TNUR') {
			header('Location: tnur.php');
			exit();
		} else if ($_SESSION['user'] == 'PHY') {
			header('Location: phy.php');
			exit();	
		}
	}
	
?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>The emergency room at Cuckoo's Nest Hospital</title>
	<meta name="description" content="bla bla bla" />
	<meta name="keywords" content="la la la" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<style type="text/css">
		#main {
			background-image: url('img/index.png');
			background-position: right;
			background-size: cover;
			text-align: left;
			height: 500px;
		}
		input[type=submit]
		{
			margin-left: 30px;
		}
		input[type=text],
		input[type=password]
		{
			margin-left: 30px;
		}
	</style>
</head>

<body>
	<div id="container">
		<div id="header">
			&#x271A; EMERGENCY ROOM &#x271A;
		</div>
		
		<div id="adl">
			
		</div>
		
		<div id="main">
		
			<form action="login.php" method="post">
			
				<input type="text" name="login" placeholder="Login: " /><br />
				<input type="password" name="password" placeholder="Password: " /><br />
				<?php
					if (isset($_SESSION['err'])) {
						echo $_SESSION['err'];
						unset($_SESSION['err']);
					}
				?>
				<br />
				<input type="submit" value="Sign in" />
			
			</form>
			
		</div>
		
		<div id="adr">
			
		</div>
		
		<div id="footer">
			&copy; Cuckoo's Nest Hospital
		</div>
	</div>
</body>

</html>