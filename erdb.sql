BEGIN;

CREATE TABLE Priority (
	ID SERIAL NOT NULL CHECK (ID < 6),
	Condition VARCHAR(20) NOT NULL,
	PRIMARY KEY (ID),
	UNIQUE (Condition)
);

CREATE TABLE EmergencyRoom (
	ID SERIAL NOT NULL,
	Hospital VARCHAR(32) NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE EmergencyTeam (
	ID SERIAL NOT NULL CHECK (ID < 6),
	Name VARCHAR(10) NOT NULL,
	ER_ID INT NOT NULL DEFAULT 1,
	PRIMARY KEY (ID),
	UNIQUE (Name, ER_ID),
	FOREIGN KEY (ER_ID) REFERENCES EmergencyRoom (ID) ON DELETE CASCADE
);

CREATE TABLE MedicalIssue (
	ID SERIAL NOT NULL,
	Name VARCHAR(32) NOT NULL,
	PRIMARY KEY (ID),
	UNIQUE (Name)
);

CREATE TABLE Patient (
	ID SERIAL NOT NULL,
	First_Name VARCHAR(32) NOT NULL,
	Last_Name VARCHAR(32) NOT NULL,
	Sex VARCHAR(1) NOT NULL,
	Age INT NOT NULL,
	Priority INT,
	Assigned_To INT,
	Arrived_At INT DEFAULT 1,
	Arrival VARCHAR(32),
	Issue INT,
	Waiting_Time INT,
	Had_To_Wait INT DEFAULT 0,
	PRIMARY KEY (ID),
	UNIQUE(First_Name, Last_Name, Sex, Age, Priority),
	FOREIGN KEY (Priority) REFERENCES Priority (ID) ON UPDATE CASCADE,
	FOREIGN KEY (Arrived_At) REFERENCES EmergencyRoom (ID) ON UPDATE CASCADE,
	FOREIGN KEY (Assigned_To) REFERENCES EmergencyTeam (ID) ON UPDATE CASCADE,
	FOREIGN KEY (Issue) REFERENCES MedicalIssue (ID) ON UPDATE CASCADE
);

CREATE TABLE Queue (
	Patient_ID INT NOT NULL,
	Place SERIAL NOT NULL,
	Emergency_Team_ID INT NOT NULL,
	PRIMARY KEY (Patient_ID),
	FOREIGN KEY (Emergency_Team_ID) REFERENCES EmergencyTeam (ID) ON UPDATE CASCADE,
	FOREIGN KEY (Patient_ID) REFERENCES Patient (ID) ON DELETE CASCADE
);

CREATE TABLE CompetentToDealWith (
	Team_ID INT NOT NULL,
	Issue_ID INT NOT NULL,
	FOREIGN KEY (Team_ID) REFERENCES EmergencyTeam (ID) ON UPDATE CASCADE,
	FOREIGN KEY (Issue_ID) REFERENCES MedicalIssue (ID) ON UPDATE CASCADE
);

CREATE TABLE MedicalProcedure (
	ID SERIAL NOT NULL,
	Name VARCHAR(32) NOT NULL,
	Issue_ID INT NOT NULL,
	Cost DECIMAL NOT NULL,
	PRIMARY KEY (ID),
	UNIQUE (Name, Issue_ID),
	FOREIGN KEY (Issue_ID) REFERENCES MedicalIssue (ID) ON UPDATE CASCADE
);

CREATE TABLE Drug (
	ID SERIAL NOT NULL,
	Name VARCHAR(32) NOT NULL,
	Cost DECIMAL NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE Treatment (
	Patient_ID INT NOT NULL,
	Team_ID INT NOT NULL,
	Cost DECIMAL DEFAULT 0,
	Outcome VARCHAR(32),
	FOREIGN KEY (Patient_ID) REFERENCES Patient (ID) ON DELETE CASCADE,
	FOREIGN KEY (Team_ID) REFERENCES EmergencyTeam (ID) ON UPDATE CASCADE
);

CREATE TABLE UtilizedProcedure (
	Patient_ID INT NOT NULL,
	Procedure_ID INT NOT NULL,
	FOREIGN KEY (Patient_ID) REFERENCES Patient (ID) ON DELETE CASCADE,
	FOREIGN KEY (Procedure_ID) REFERENCES MedicalProcedure (ID) ON UPDATE CASCADE
);

CREATE TABLE ProvidedDrug (
	Patient_ID INT NOT NULL,
	Drug_ID INT NOT NULL,
	FOREIGN KEY (Patient_ID) REFERENCES Patient (ID) ON DELETE CASCADE,
	FOREIGN KEY (Drug_ID) REFERENCES Drug (ID) ON UPDATE CASCADE
);

-- Some views
CREATE VIEW Priorityqueue AS
SELECT *  
FROM Patient, Queue
WHERE Patient.ID = Queue.Patient_ID
ORDER BY Patient.Priority DESC, Queue.Place;

-- VitalStatistics    
-- 1 BOTH SOME PROCEDURE(s) AND DRUG(s) PROVIDED    
-- 2 NO DRUGS PROVIDED (ONLY PROCEDURE(s))
-- 3 NO PROCEDURES UTILIZED (ONLY DRUG(s))
-- 4 NEITHER PROCEDURE(s) NOR DRUG(s) = MALINGER!!! 
CREATE VIEW VitalStatistics1 AS
SELECT Patient.ID, First_Name, Last_Name, Sex, Age, MedicalIssue.Name AS Issue, Had_To_Wait, MedicalProcedure.Name AS Procedure, Drug.Name AS Drug, Treatment.Cost
FROM (((((Patient INNER JOIN Treatment ON Patient.ID = Treatment.Patient_ID) 
	INNER JOIN UtilizedProcedure ON Patient.ID = UtilizedProcedure.Patient_ID)
		INNER JOIN ProvidedDrug ON Patient.ID = ProvidedDrug.Patient_ID) 
			INNER JOIN MedicalIssue ON Patient.Issue = MedicalIssue.ID) 
				INNER JOIN MedicalProcedure ON UtilizedProcedure.Procedure_ID = MedicalProcedure.ID) 
					INNER JOIN Drug ON ProvidedDrug.Drug_ID = Drug.ID;
					
CREATE VIEW VitalStatistics2 AS
SELECT Patient.ID, First_Name, Last_Name, Sex, Age, MedicalIssue.Name AS Issue, Had_To_Wait, MedicalProcedure.Name AS Procedure, Treatment.Cost
FROM (((Patient INNER JOIN Treatment ON Patient.ID = Treatment.Patient_ID) 
	INNER JOIN UtilizedProcedure ON Patient.ID = UtilizedProcedure.Patient_ID)
		INNER JOIN MedicalIssue ON Patient.Issue = MedicalIssue.ID) 
			INNER JOIN MedicalProcedure ON UtilizedProcedure.Procedure_ID = MedicalProcedure.ID;
					
CREATE VIEW VitalStatistics3 AS
SELECT Patient.ID, First_Name, Last_Name, Sex, Age, MedicalIssue.Name AS Issue, Had_To_Wait, Drug.Name AS Drug, Treatment.Cost
FROM (((Patient INNER JOIN Treatment ON Patient.ID = Treatment.Patient_ID) 
	INNER JOIN ProvidedDrug ON Patient.ID = ProvidedDrug.Patient_ID) 
		INNER JOIN MedicalIssue ON Patient.Issue = MedicalIssue.ID) 
			INNER JOIN Drug ON ProvidedDrug.Drug_ID = Drug.ID;

CREATE VIEW VitalStatistics4 AS
SELECT Patient.ID, First_Name, Last_Name, Sex, Age, MedicalIssue.Name AS Issue, Had_To_Wait, Treatment.Cost
FROM (Patient INNER JOIN Treatment ON Patient.ID = Treatment.Patient_ID) 
	INNER JOIN MedicalIssue ON Patient.Issue = MedicalIssue.ID;
			
COMMIT;

BEGIN;

CREATE FUNCTION toWait() RETURNS TRIGGER AS 
$$
DECLARE
	o_time INT;
	n_time INT;
BEGIN
	SELECT Had_To_Wait INTO o_time FROM Patient WHERE OLD.ID = NEW.ID;
	SELECT Waiting_Time INTO n_time FROM Patient WHERE OLD.ID = NEW.ID;
	IF NEW.Waiting_Time > NEW.Had_To_Wait THEN 
		UPDATE Patient SET Had_To_Wait = NEW.Waiting_Time WHERE ID = NEW.ID;
	END IF;
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER TotalWaitingTime AFTER UPDATE OF Waiting_Time ON Patient
FOR EACH ROW EXECUTE PROCEDURE toWait();

CREATE FUNCTION toTreat() RETURNS TRIGGER AS 
$$
BEGIN
	INSERT INTO Treatment(Patient_ID, Team_ID) VALUES (NEW.ID, NEW.Assigned_To);
	RETURN NEW; 
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER BeginTreatment AFTER INSERT ON Patient
FOR EACH ROW EXECUTE PROCEDURE toTreat();

CREATE FUNCTION toCalculate() RETURNS TRIGGER AS 
$$
DECLARE 
	new_cost DECIMAL;
BEGIN
	SELECT Cost INTO new_cost FROM MedicalProcedure WHERE ID = NEW.Procedure_ID;
	UPDATE Treatment SET Cost = Cost+new_cost WHERE Patient_ID = NEW.Patient_ID;
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';    

CREATE TRIGGER UpdateCost AFTER INSERT ON UtilizedProcedure
FOR EACH ROW EXECUTE PROCEDURE toCalculate();

CREATE FUNCTION toCalculate2() RETURNS TRIGGER AS 
$$
DECLARE 
	new_cost DECIMAL;
BEGIN
	SELECT Cost INTO new_cost FROM Drug WHERE ID = NEW.Drug_ID;
	UPDATE Treatment SET Cost = Cost+new_cost WHERE Patient_ID = NEW.Patient_ID;
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';    

CREATE TRIGGER UpdateCost2 AFTER INSERT ON ProvidedDrug
FOR EACH ROW EXECUTE PROCEDURE toCalculate2();

CREATE FUNCTION toEnqueue() RETURNS TRIGGER AS 
$$
BEGIN
	INSERT INTO Queue(Patient_ID, Emergency_Team_ID) VALUES (NEW.ID, NEW.Assigned_To);
	RETURN NEW; 
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER Enqueue AFTER INSERT ON Patient
FOR EACH ROW EXECUTE PROCEDURE toEnqueue();

CREATE FUNCTION toApproximate() RETURNS TRIGGER AS 
$$
DECLARE
	tuple3 RECORD;
	awaiting_time INT;
BEGIN
	FOR team IN 1..5 LOOP
		awaiting_time := 0;
		FOR tuple3 IN
			SELECT * FROM Priorityqueue WHERE Emergency_Team_ID = team -- 1 'ALFA' 2 'BETA' 3 'GAMMA' 4 'DELTA' 5 'EPSILON'
		LOOP
			UPDATE Patient SET Waiting_Time = awaiting_time WHERE ID = tuple3.Patient_ID;
			awaiting_time := awaiting_time+10;
		END LOOP;
	END LOOP;
	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER TimeApproximation AFTER INSERT ON Queue
FOR EACH ROW EXECUTE PROCEDURE toApproximate();

COMMIT;

BEGIN;

INSERT INTO EmergencyRoom(Hospital) VALUES ('CNH');

INSERT INTO Priority(Condition) VALUES ('SATISFACTORY');
INSERT INTO Priority(Condition) VALUES ('GUARDED');
INSERT INTO Priority(Condition) VALUES ('SERIOUS');
INSERT INTO Priority(Condition) VALUES ('CRITICAL');
INSERT INTO Priority(Condition) VALUES ('GRAVE');

INSERT INTO EmergencyTeam(Name) VALUES ('ALFA');
INSERT INTO EmergencyTeam(Name) VALUES ('BETA');
INSERT INTO EmergencyTeam(Name) VALUES ('GAMMA');
INSERT INTO EmergencyTeam(Name) VALUES ('DELTA');
INSERT INTO EmergencyTeam(Name) VALUES ('EPSILON');

INSERT INTO MedicalIssue(Name) VALUES ('headaches');					
INSERT INTO MedicalIssue(Name) VALUES ('foreign_objects_in_the_body');
INSERT INTO MedicalIssue(Name) VALUES ('skin_infections');
INSERT INTO MedicalIssue(Name) VALUES ('back_pain');
INSERT INTO MedicalIssue(Name) VALUES ('cuts_and_contusions');
INSERT INTO MedicalIssue(Name) VALUES ('upper_respiratory_infections');
INSERT INTO MedicalIssue(Name) VALUES ('sprains_and_broken_bones');
INSERT INTO MedicalIssue(Name) VALUES ('toothaches');
INSERT INTO MedicalIssue(Name) VALUES ('abdominal_pain');
INSERT INTO MedicalIssue(Name) VALUES ('chest_pains');

INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Randle', 'McMurphy', 'M', 38, 'independently', 5, 1, 1);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Chief', 'Bromden', 'M', 42, 'by_ambulance', 5, 2, 3);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Mildred', 'Ratched', 'F', 41, 'independently', 5, 3, 4);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Billy', 'Bibbit', 'M', 25, 'by_ambulance', 4, 4, 1);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Dale', 'Harding', 'M', 62, 'by_ambulance', 4, 5, 6);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('George', 'Sorensen', 'M', 58, 'independently', 3, 2, 6);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Charlie', 'Cheswick', 'M', 61, 'by_ambulance', 4, 1, 2);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Martini', 'Martini', 'M', 34, 'independently', 3, 3, 9);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Scanlon', 'Scanlon', 'M', 41, 'independently', 2, 4, 5);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Max', 'Taber', 'M', 29, 'independently', 4, 5, 9);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Jim', 'Sefelt', 'M', 72, 'by_ambulance', 2, 1, 8);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Bruce', 'Fredrickson', 'M', 59, 'independently', 2, 5, 10);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Pete', 'Bancini', 'M', 74, 'independently', 1, 1, 1);
INSERT INTO Patient(First_Name, Last_Name, Sex, Age, Arrival, Priority, Assigned_To, Issue) VALUES ('Colonel', 'Matterson', 'M', 88, 'by_ambulance', 1, 1, 2);

INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (1, 1);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (1, 2);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (1, 8);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (2, 3);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (2, 6);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (2, 9);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (3, 4);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (3, 9);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (3, 10);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (4, 1);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (4, 5);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (4, 7);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (5, 6);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (5, 9);
INSERT INTO CompetentToDealWith(Team_ID, Issue_ID) VALUES (5, 10);

INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Ventriculostomy', 1, 550);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Laryngoscopy', 1, 780);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Computed_tomography', 1, 560);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Ultrasonography', 2, 880);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Gastric_lavage', 2, 780);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Paracentesis', 2, 210);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Escharotomy', 3, 470);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Drainage', 3, 930);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Incision', 3, 670);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Lumbar_puncture', 4, 110);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Magnetic_resonance_imaging', 4, 430);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Arthrocentesis', 4, 230);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Biopsy', 5, 340);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Blood_transfusion', 5, 540);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('EEG_electroencephalogram', 5, 340);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Allergy_shots', 6, 340);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Acupuncture', 6, 980);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Keratectomy', 6, 470);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Transfusion', 7, 460);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Echocardiogram', 7, 540);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Duodenoscopy', 7, 340);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('X-rays', 8, 100);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Colonoscopy', 8, 900);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Wound_closure', 8, 80);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Gastroscopy', 9, 500);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Bypass', 9, 400);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Appendectomy', 9, 300);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Vasectomy', 10, 200);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Tracheostomy', 10, 100);
INSERT INTO MedicalProcedure(Name, Issue_ID, Cost) VALUES ('Dialysis', 10, 400);

INSERT INTO Drug(Name, Cost) VALUES ('Hydrocortisone_sodium', 5);
INSERT INTO Drug(Name, Cost) VALUES ('Adrenaline', 53);
INSERT INTO Drug(Name, Cost) VALUES ('Chlorphenamine', 21);
INSERT INTO Drug(Name, Cost) VALUES ('Paracetamol', 14);
INSERT INTO Drug(Name, Cost) VALUES ('Dihydrocodeine', 33);
INSERT INTO Drug(Name, Cost) VALUES ('Cyclimorph', 23);
INSERT INTO Drug(Name, Cost) VALUES ('Diclofenac', 26);
INSERT INTO Drug(Name, Cost) VALUES ('Naloxone', 19);
INSERT INTO Drug(Name, Cost) VALUES ('Amoxicillin', 16);
INSERT INTO Drug(Name, Cost) VALUES ('Penicillin', 20);
INSERT INTO Drug(Name, Cost) VALUES ('Aspirin', 13);
INSERT INTO Drug(Name, Cost) VALUES ('Atropine', 23);
INSERT INTO Drug(Name, Cost) VALUES ('Diazepam', 48);
INSERT INTO Drug(Name, Cost) VALUES ('Chlorpromazine', 24);
INSERT INTO Drug(Name, Cost) VALUES ('Dopamine', 80);

COMMIT;