function timer()
{
	var today = new Date();

	var day = today.getDate();
	if (day<10) day = "0"+day;
	var month = today.getMonth()+1;
	if (month<10) month = "0"+month;
	var year = today.getFullYear();
	
	var hour = today.getHours();
	if (hour<10) hour = "0"+hour;
	
	var minute = today.getMinutes();
	if (minute<10) minute = "0"+minute;
	
	var second = today.getSeconds();
	if (second<10) second = "0"+second;
	
	document.getElementById("time").innerHTML = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;
		
	setTimeout("timer()", 1000);
}

$(document).ready(function() {
	var NavY = $('#header').offset().top;
	
	var stickyNav = function(){
		var ScrollY = $(window).scrollTop();
		
		if (ScrollY > NavY) {
			$('#header').addClass('sticky');
		} else {
			$('#header').removeClass('sticky');
		}
	};
	
	stickyNav();
	
	$(window).scroll(function() {
		stickyNav();
	});
});

function scroll_to(selector) {
    $('html,body').animate({scrollTop: $(selector).offset().top-160}, 2000);
    return false;
};

setInterval(function(){
	$("#blinker").toggleClass("blinking");
},1000)
