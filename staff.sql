CREATE TABLE Staff (
	ID SERIAL NOT NULL,
	Role VARCHAR(32) NOT NULL,
	Login VARCHAR(32) NOT NULL,
	Password VARCHAR(255) NOT NULL,
	Team_ID INT,
	Team VARCHAR(10) DEFAULT 'NONE',
	PRIMARY KEY (ID)
);

BEGIN;

INSERT INTO Staff(Role, Login, Password) VALUES ('triage nurse','TNUR','$2y$10$K40ny5Qx0PK3KKNSixqOWeqVX2zbV3ktAwoUGoUc87/wljPv8kyni');
INSERT INTO Staff(Role, Login, Password, Team_ID, Team) VALUES ('physician','APHY','$2y$10$IQ6UG236MPKvpdK0vFFwduHWSeAMULI8DaenEx8t.Gl0GpKZAdkXi', 1, 'ALFA');
INSERT INTO Staff(Role, Login, Password, Team_ID, Team) VALUES ('physician','BPHY','$2y$10$BgLZQAQ0FL7GOoAX.a82xubBMOTmVa1xV1hClcD0aZZSnWf2AGEVS', 2, 'BETA');
INSERT INTO Staff(Role, Login, Password, Team_ID, Team) VALUES ('physician','GPHY','$2y$10$DVwtgzfb87HHkkZSPTCFR.CoL27nZX7aB9q9ziEg/mgFnp4OhJuIW', 3, 'GAMMA');
INSERT INTO Staff(Role, Login, Password, Team_ID, Team) VALUES ('physician','DPHY','$2y$10$bT0e4ipbs/Iz52v2kYvLPu8oI1fzE11RsRCtY1dIk0i6o/6iC.nQW', 4, 'DELTA');
INSERT INTO Staff(Role, Login, Password, Team_ID, Team) VALUES ('physician','EPHY','$2y$10$GTR4oz2l9LqD4OPRhunAye.MhBNM7CvgkT6sfQ4dldTF6nxmj7uZC', 5, 'EPSILON');

COMMIT;