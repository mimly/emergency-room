<?php

session_start();

if ((!isset($_POST['login'])) or (!isset($_POST['password']))) {
	header('Location: index.php');
	exit();
}

require_once "connect.php";

$connection = pg_connect("$host $port $dbname $credentials");

if (!$connection) {
	echo "Error : Unable to open database\n";
} else {
	echo "Opened database successfully\n";
	
	$login = $_POST['login'];
	$pwd = $_POST['password'];
	
	$login = htmlentities($login, ENT_QUOTES, "UTF-8");
	
	$sql = sprintf("SELECT * FROM Staff WHERE Login='%s'",
		pg_escape_string($connection, $login));
	
	if ($result = pg_query($connection, $sql)) {
		
		$auth = pg_num_rows($result);
		if ($auth == 1) {
			
			$row = pg_fetch_row($result);
			
			if (password_verify($pwd, $row[3])) {
				
				unset($_SESSION['err']);
				$_SESSION['loggedIn'] = true;
				$_SESSION['user'] = $login;
				
				
				$_SESSION['role'] = $row[1];
				$_SESSION['team_id'] = $row[4];
				$_SESSION['team'] = $row[5];
				if ($login == 'TNUR') {
					header('Location: tnur.php');
				} else {
					header('Location: phy.php');
				}
				
			} else {
				
				$_SESSION['err'] = '<span style="color:red">Login name or password is incorrect.</span>';
				header('Location: index.php');
				
			}
			
		} else {
			
			$_SESSION['err'] = '<span style="color:red">Login name or password is incorrect.</span>';
			header('Location: index.php');
			
		}
		
	}
	
	pg_close($connection);
}

?>