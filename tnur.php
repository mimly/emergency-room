<?php

	session_start();
	
	if (!isset($_SESSION['loggedIn'])) {
		header('Location: index.php');
		exit();
	}
	
?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>The emergency room at Cuckoo's Nest Hospital</title>
	<meta name="description" content="bla bla bla" />
	<meta name="keywords" content="la la la" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<script type="text/javascript" src="jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="script.js"></script>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body onload="timer();">
	<div id="container">
		<div id="header">
			&#x271A; EMERGENCY ROOM &#x271A;
		</div>
		
		<div id="adl">
			 
		</div>
		
		<div id="main">
		
			<div id="main_header">
				
				<div id="main_header_form">
					<div id="time"></div>
					<?php
				
						echo "Logged in as a ".$_SESSION['role'].". [<a href='logout.php'>Sign out.</a>]";
				
					?>
				</div>
			
			</div>
			
			<form id="main_form" action="tnur.php" method="get" > 
			
				<div id="main_left">
				
					<fieldset id="priority">
						<legend>Priority:</legend>
						<div id="main_left_form">
							<input id="main_left_1" type="radio" name="priority" value="1" required />
							<label for="main_left_1">SATISFACTORY</label>
							<input id="main_left_2" type="radio" name="priority" value="2" />
							<label for="main_left_2">GUARDED</label>
							<input id="main_left_3" type="radio" name="priority" value="3" />
							<label for="main_left_3">SERIOUS</label>
							<input id="main_left_4" type="radio" name="priority" value="4" />
							<label for="main_left_4">CRITICAL</label>
							<input id="main_left_5" type="radio" name="priority" value="5" />
							<label for="main_left_5">GRAVE</label>			
						</div>
					</fieldset>
				
				</div>
				<div id="main_center">
	
						<fieldset id="personal_info">
							<legend>Personal information:</legend>
							<div id="main_center_form">
								<input type="text" name="name" pattern="[A-Za-z ]+" placeholder="First name: " maxlength=32 minlength=2 required /><br />
								<input type="text" name="surname" pattern="[A-Za-z ]+" placeholder="Last name: " maxlength=32 minlength=2 required /><br />
								<input type="radio" id="r1" name="sex" value="M" required />
								<label for="r1">MALE</label>
								<input type="radio" id="r2" name="sex" value="F" />
								<label for="r2">FEMALE</label><br />
								<input type="number" name="age" min="1" max="120" placeholder="Age: " required />
							</div>
							<div id="main_center_buttons">
								<input id="button" type="submit" value="PROCEED" /><input id="button" type="reset" value="RESET"/>
							</div>
						</fieldset>
				
				</div>
				<div id="main_right">
				
					<fieldset id="medical_issue">
						<legend>Medical issue:</legend>
						<div id="main_right_form">
							<input id="main_right_0" type="radio" name="m_issue" value="1" required />
							<label for="main_right_0">headaches</label>
							<input id="main_right_1" type="radio" name="m_issue" value="2" />
							<label for="main_right_1">foreign objects in the body</label>
							<input id="main_right_2" type="radio" name="m_issue" value="3" />
							<label for="main_right_2">skin infections</label>
							<input id="main_right_3" type="radio" name="m_issue" value="4" />
							<label for="main_right_3">back pain</label>
							<input id="main_right_4" type="radio" name="m_issue" value="5" />
							<label for="main_right_4">cuts and contusions</label>
							<input id="main_right_5" type="radio" name="m_issue" value="6" />
							<label for="main_right_5">upper respiratory infections</label>
							<input id="main_right_6" type="radio" name="m_issue" value="7" />
							<label for="main_right_6">sprains and broken bones</label>
							<input id="main_right_7" type="radio" name="m_issue" value="8" />
							<label for="main_right_7">toothaches</label>
							<input id="main_right_8" type="radio" name="m_issue" value="9" />
							<label for="main_right_8">abdominal pain</label>
							<input id="main_right_9" type="radio" name="m_issue" value="10" />
							<label for="main_right_9">chest pains</label>
						</div>
					</fieldset>
				
				</div>
				
			</form>
			<div id="main_footer">
			
				<div id="teams">
					<fieldset id="eteams">
						<legend>Emergency teams:</legend>
					
						<fieldset id="alfa">
							<legend>ALFA</legend>
							
							<?php
								//$competent = array_fill(0, 5, 'disabled');
								$_SESSION['competent'] = array_fill(0, 5, 'disabled');
								$_SESSION['medical_issue'] = $_GET['m_issue'];
								echo "<iframe src='queue.php?team=1' height='200' width='250' ></iframe>";
							?>
							
						</fieldset>
						
						<fieldset id="beta">
							<legend>BETA</legend>
							
							<?php
								echo "<iframe src='queue.php?team=2' height='200' width='250' ></iframe>";						
							?>
								
						</fieldset>
						
						<fieldset id="gamma">
							<legend>GAMMA</legend>
							
							<?php
								echo "<iframe src='queue.php?team=3' height='200' width='250' ></iframe>";						
							?>
								
						</fieldset>
						
						<fieldset id="delta">
							<legend>DELTA</legend>
							
							<?php
								echo "<iframe src='queue.php?team=4' height='200' width='250' ></iframe>";				
							?>
							
						</fieldset>
						
						<fieldset id="epsilon">
							<legend>EPSILON</legend>
							
							<?php
								echo "<iframe src='queue.php?team=5' height='200' width='250' ></iframe>";				
							?>
								
						</fieldset>
							
					</fieldset>
				</div>
				<fieldset id="assignment">
					<legend>Assignment form</legend>
					
					<?php
						
						$_SESSION['first_name'] = $_GET['name'];
						$_SESSION['last_name'] = $_GET['surname'];
						$_SESSION['sex'] = $_GET['sex'];
						$_SESSION['age'] = $_GET['age'];
						$_SESSION['priority'] = $_GET['priority'];
						
						echo "<iframe src='assign.php' height='200' width='500' scrolling='no' frameborder=0 allign='middle' ></iframe>";	
		
					?>
							
				</fieldset>
			
			</div>
			
		</div>
		
		<div id="adr">
			
		</div>
		
		<div id="footer">
			&copy; Cuckoo's Nest Hospital
		</div>
	
	</div>
</body>

</html>