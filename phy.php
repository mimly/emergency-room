<?php

	session_start();
	
	if (!isset($_SESSION['loggedIn'])) {
		header('Location: index.php');
		exit();
	}

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>The emergency room at Cuckoo's Nest Hospital</title>
	<meta name="description" content="bla bla bla" />
	<meta name="keywords" content="la la la" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<script type="text/javascript" src="jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="script.js"></script>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body onload="timer();">
	<div id="container">
		<div id="header">
			&#x271A; EMERGENCY ROOM &#x271A;
		</div>
		
		<div id="adl">
			
		</div>
		
		<div id="main">
		
			<div id="main_header">
				
				<div id="main_header_form">
					<div id="time"></div>
					<?php
				
						echo "Logged in as a ".$_SESSION['team']."-".$_SESSION['role'].". [<a href='logout.php'>Sign out.</a>]";
				
					?>
				</div>
			
			</div>
			
			<?php
					
				require_once "connect.php";

				$connection = pg_connect("$host $port $dbname $credentials");
				
				$sql = sprintf("SELECT * FROM Priorityqueue WHERE emergency_team_id = '%s'",
					pg_escape_string($connection, $_SESSION['team_id'])); // 1 'ALFA' 2 'BETA' 3 'GAMMA' 4 'DELTA' 5 'EPSILON'
				$result = pg_query($connection, $sql);
				
				if (pg_num_rows($result) > 0) {
					$row = pg_fetch_row($result);
					$_SESSION['medical_issue'] = $row[9];
					$_SESSION['patient_id'] = $row[12];
					$the_current_patient = "$row[1] $row[2]";
				} else {
					$the_current_patient = 'NONE';
				}
				
				pg_close($connection);
				
			?>
	
			<form id="main_form" action="submit.php" method="post" >
			
				<fieldset id="phy">
					<?php 
						echo "<legend>The current patient is: <span style='color:darkred'>$the_current_patient</span></legend>";
					?>
					<fieldset id="m_procedure">
						<?php
								
							require_once "connect.php";

							$connection = pg_connect("$host $port $dbname $credentials");
						
							$sql = sprintf("SELECT ID FROM MedicalProcedure WHERE issue_id = '%s'",
								pg_escape_string($connection, $_SESSION['medical_issue']));
							$result = pg_query($connection, $sql);
						
							$disable = array_fill(0, 30, 'disabled');
							while ($row = pg_fetch_row($result)) {
								$disable[$row[0]-1] = '';
							}
						
							pg_close($connection);
							
							echo<<<END
							
							<legend>Medical procedure(s):</legend>
							<div id="mps1">
								<input id="mp1" type="checkbox" name="procedures[]" value="1" $disable[0] />
								<label for="mp1">Ventriculostomy</label>
								<input id="mp2" type="checkbox" name="procedures[]" value="2" $disable[1] />
								<label for="mp2">Laryngoscopy</label>
								<input id="mp3" type="checkbox" name="procedures[]" value="3" $disable[2] />
								<label for="mp3">Computed tomography</label>
								<input id="mp4" type="checkbox" name="procedures[]" value="4" $disable[3] />
								<label for="mp4">Ultrasonography</label>
								<input id="mp5" type="checkbox" name="procedures[]" value="5" $disable[4] />
								<label for="mp5">Gastric lavage</label>
								<input id="mp6" type="checkbox" name="procedures[]" value="6" $disable[5] />
								<label for="mp6">Paracentesis</label>
							</div>
							<div id="mps2">
								<input id="mp7" type="checkbox" name="procedures[]" value="7" $disable[6] />
								<label for="mp7">Escharotomy</label>	
								<input id="mp8" type="checkbox" name="procedures[]" value="8" $disable[7] />
								<label for="mp8">Drainage</label>	
								<input id="mp9" type="checkbox" name="procedures[]" value="9" $disable[8] />
								<label for="mp9">Incision</label>	
								<input id="mp10" type="checkbox" name="procedures[]" value="10" $disable[9] />
								<label for="mp10">Lumbar puncture</label>
								<input id="mp11" type="checkbox" name="procedures[]" value="11" $disable[10] />
								<label for="mp11">Magnetic resonance</label>	
								<input id="mp12" type="checkbox" name="procedures[]" value="12" $disable[11] />
								<label for="mp12">Arthrocentesis</label>	
							</div>
							<div id="mps3">
								<input id="mp13" type="checkbox" name="procedures[]" value="13" $disable[12] />
								<label for="mp13">Biopsy</label>	
								<input id="mp14" type="checkbox" name="procedures[]" value="14" $disable[13] />
								<label for="mp14">Blood transfusion</label>	
								<input id="mp15" type="checkbox" name="procedures[]" value="15" $disable[14] />
								<label for="mp15">EEG</label>
								<input id="mp16" type="checkbox" name="procedures[]" value="16" $disable[15] />
								<label for="mp16">Allergy shots</label>	
								<input id="mp17" type="checkbox" name="procedures[]" value="17" $disable[16] />
								<label for="mp17">Acupuncture</label>	
								<input id="mp18" type="checkbox" name="procedures[]" value="18" $disable[17] />
								<label for="mp18">Keratectomy</label>	
							</div>
							<div id="mps4">
								<input id="mp19" type="checkbox" name="procedures[]" value="19" $disable[18] />
								<label for="mp19">Transfusion</label>	
								<input id="mp20" type="checkbox" name="procedures[]" value="20" $disable[19] />
								<label for="mp20">Echocardiogram</label>
								<input id="mp21" type="checkbox" name="procedures[]" value="21" $disable[20] />
								<label for="mp21">Duodenoscopy</label>	
								<input id="mp22" type="checkbox" name="procedures[]" value="22" $disable[21] />
								<label for="mp22">X-rays</label>	
								<input id="mp23" type="checkbox" name="procedures[]" value="23" $disable[22] />
								<label for="mp23">Colonoscopy</label>	
								<input id="mp24" type="checkbox" name="procedures[]" value="24" $disable[23] />
								<label for="mp24">Wound closure</label>	
							</div>
							<div id="mps5">
								<input id="mp25" type="checkbox" name="procedures[]" value="25" $disable[24] />
								<label for="mp25">Gastroscopy</label>
								<input id="mp26" type="checkbox" name="procedures[]" value="26" $disable[25] />
								<label for="mp26">Bypass</label>	
								<input id="mp27" type="checkbox" name="procedures[]" value="27" $disable[26] />
								<label for="mp27">Appendectomy</label>	
								<input id="mp28" type="checkbox" name="procedures[]" value="28" $disable[27] />
								<label for="mp28">Vasectomy</label>	
								<input id="mp29" type="checkbox" name="procedures[]" value="29" $disable[28] />
								<label for="mp29">Tracheostomy</label>	
								<input id="mp30" type="checkbox" name="procedures[]" value="30" $disable[29] />
								<label for="mp30">Dialysis</label>
							</div>
END;
						?>
					</fieldset>
					
					<fieldset id="drug">
						<legend>Drug(s):</legend>
						<div id="ds1">
							<input id="d1" type="checkbox" name="drugs[]" value="1" />
							<label for="d1">Hydrocortisone sodium</label>
							<input id="d2" type="checkbox" name="drugs[]" value="2" />
							<label for="d2">Adrenaline</label>
							<input id="d3" type="checkbox" name="drugs[]" value="3" />
							<label for="d3">Chlorphenamine</label>
						</div>
						<div id="ds2">
							<input id="d4" type="checkbox" name="drugs[]" value="4" />
							<label for="d4">Paracetamol</label>
							<input id="d5" type="checkbox" name="drugs[]" value="5" />
							<label for="d5">Dihydrocodeine</label>
							<input id="d6" type="checkbox" name="drugs[]" value="6" />
							<label for="d6">Cyclimorph</label>
						</div>
						<div id="ds3">
							<input id="d7" type="checkbox" name="drugs[]" value="7" />
							<label for="d7">Diclofenac</label>
							<input id="d8" type="checkbox" name="drugs[]" value="8" />
							<label for="d8">Naloxone</label>
							<input id="d9" type="checkbox" name="drugs[]" value="9" />
							<label for="d9">Amoxicillin</label>
						</div>
						<div id="ds4">
							<input id="d10" type="checkbox" name="drugs[]" value="10" />
							<label for="d10">Penicillin</label>
							<input id="d11" type="checkbox" name="drugs[]" value="11" />
							<label for="d11">Aspirin</label>
							<input id="d12" type="checkbox" name="drugs[]" value="12" />
							<label for="d12">Atropine</label>
						</div>
						<div id="ds5">
							<input id="d13" type="checkbox" name="drugs[]" value="13" />
							<label for="d13">Diazepam</label>
							<input id="d14" type="checkbox" name="drugs[]" value="14" />
							<label for="d14">Chlorpromazine</label>
							<input id="d15" type="checkbox" name="drugs[]" value="15" />
							<label for="d15">Dopamine</label>
						</div>
					</fieldset>
					
					<div id="main_footer">
					
						<input id="COMMITTED" type="radio" name="outcome" value="commited" required />
						<label for="COMMITTED">COMMITTED</label>
						<input id="RELEASED" type="radio" name="outcome" value="released" />
						<label for="RELEASED">RELEASED</label>
						<input id="button" type="submit" value="SUBMIT" /><input id="button" type="reset" value="RESET" />
					
					</div>
					
					<?php
						if (isset($_SESSION['submit'])) {
							echo $_SESSION['submit'];
							unset($_SESSION['submit']);
						}
					?>
					
				</fieldset>
				
			</form>
			
		</div>
		
		<div id="adr">
			
		</div>
		
		<div id="footer">
			&copy; Cuckoo's Nest Hospital
		</div>
	</div>
</body>

</html>